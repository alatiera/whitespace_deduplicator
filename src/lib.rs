/// Small utility that replaces multiple whitespace occurrences with a single space.
// TODO: use CoW
// TODO: Instead of filtering all empty newlines, preserve the first and discard the rest
pub fn replace_extra_spaces(s: &str) -> String {
    s.lines()
     .map(|x| x.split_whitespace().collect::<Vec<_>>().join(" "))
     .filter(|x| !x.is_empty())
     .collect::<Vec<_>>().join("\n")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extra_spaces() {
        let bad_txt = "1   2   3        4  5";
        let valid_txt = "1 2 3 4 5";
        assert_eq!(replace_extra_spaces(&bad_txt), valid_txt);
    }

    #[test]
    fn test_single_newline() {
        let bad_txt = "1   2   3  \n      4  5\n";
        let valid_txt = "1 2 3\n4 5";
        assert_eq!(replace_extra_spaces(&bad_txt), valid_txt);
    }

    #[test]
    fn test_newlines() {
        let bad_txt = "1   2   3  \n\n\n    \n  4  5\n";
        let valid_txt = "1 2 3\n4 5";
        assert_eq!(replace_extra_spaces(&bad_txt), valid_txt);
    }
}
